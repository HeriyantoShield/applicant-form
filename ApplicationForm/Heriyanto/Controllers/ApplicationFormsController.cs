﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Heriyanto;

namespace Heriyanto.Controllers
{
    public class ApplicationFormsController : Controller
    {
        private applicant_dbEntities db = new applicant_dbEntities();

        // GET: ApplicationForms
        public ActionResult Index()
        {
            return View(db.ApplicationForms.ToList());
        }

        // GET: ApplicationForms/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationForm applicationForm = db.ApplicationForms.Find(id);
            if (applicationForm == null)
            {
                return HttpNotFound();
            }
            return View(applicationForm);
        }

        // GET: ApplicationForms/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ApplicationForms/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name,Mobile_Phone_Number,Alternative_Phone_Number,Email,Place_of_Birth,Date_of_Birth,Last_Education,Collage_University,Major,Position_Apply,Event,Career_Ceenter,Referral_Name,Referral_Mobile_Phone_Number,Referral_Email")] ApplicationForm applicationForm)
        {
            if (ModelState.IsValid)
            {
                db.ApplicationForms.Add(applicationForm);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(applicationForm);
        }

        // GET: ApplicationForms/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationForm applicationForm = db.ApplicationForms.Find(id);
            if (applicationForm == null)
            {
                return HttpNotFound();
            }
            return View(applicationForm);
        }

        // POST: ApplicationForms/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Name,Mobile_Phone_Number,Alternative_Phone_Number,Email,Place_of_Birth,Date_of_Birth,Last_Education,Collage_University,Major,Position_Apply,Event,Career_Ceenter,Referral_Name,Referral_Mobile_Phone_Number,Referral_Email")] ApplicationForm applicationForm)
        {
            if (ModelState.IsValid)
            {
                db.Entry(applicationForm).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(applicationForm);
        }

        // GET: ApplicationForms/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationForm applicationForm = db.ApplicationForms.Find(id);
            if (applicationForm == null)
            {
                return HttpNotFound();
            }
            return View(applicationForm);
        }

        // POST: ApplicationForms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ApplicationForm applicationForm = db.ApplicationForms.Find(id);
            db.ApplicationForms.Remove(applicationForm);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
