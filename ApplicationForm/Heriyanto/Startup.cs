﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Heriyanto.Startup))]
namespace Heriyanto
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
